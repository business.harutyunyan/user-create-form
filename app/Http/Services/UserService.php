<?php


namespace App\Http\Services;


use App\Exceptions\SomethingWentWrongException;
use App\User;

class UserService
{
    /**
     * @param $name
     * @param $email
     * @param $website
     * @param $sex
     * @return User
     * @throws SomethingWentWrongException
     */
    public function create($name, $email, $website, $sex)
    {
        $user = new User([
            'name' => $name,
            'email' => $email,
            'website' => $website,
            'sex' => $sex,
        ]);

        if (!$user->save()) {
            throw new SomethingWentWrongException();
        }

        return $user;
    }
}
