<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    const STATUS_SUCCESS = 1;
    const STATUS_ERROR = 0;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
