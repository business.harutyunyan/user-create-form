<?php

namespace App\Http\Controllers;

use App\Exceptions\SomethingWentWrongException;
use App\Http\Requests\User\CreateRequest;
use App\Http\Services\UserService;

class UserController extends Controller
{
    private $userService;

    /**
     * UserController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createAction()
    {
        return view('user.create');
    }

    /**
     * @param CreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeAction(CreateRequest $request)
    {
        try {
            $user = $this->userService->create($request->name, $request->email, $request->website, $request->sex);
        } catch (SomethingWentWrongException $exception) {
            return redirect()->back()->with([
                'success' => self::STATUS_ERROR,
                'message' => __('messages.userNotCreated'),
            ]);
        }

        return redirect()->back()->with([
            'success' => self::STATUS_SUCCESS,
            'message' => sprintf(__('messages.userCreatedSuccessfully'), $user->name),
        ]);
    }
}
