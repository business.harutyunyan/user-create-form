<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<div class="container">
    @if(session('success') == \App\Http\Controllers\Controller::STATUS_SUCCESS)
        <div class="form-group col-lg-10">
            <div class="alert alert-success">
                <strong>Success!</strong> {{session('message')}}
            </div>
        </div>
    @endif
    @if ($errors->any())
        <div class="form-group col-lg-10">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
{!! Form::open(['route' => 'user.store', 'class' => 'form-horizontal']) !!}
<fieldset>
    <!-- Name -->
    <div class="form-group">
        {!! Form::label('name', 'Name', ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! Form::text('name', $value = null, ['class' => 'form-control', 'placeholder' => 'name']) !!}
        </div>
    </div>
    <!-- Email -->
    <div class="form-group">
        {!! Form::label('email', 'Email', ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10 has-error">
            {!! Form::email('email', $value = null, ['class' => 'form-control', 'placeholder' => 'email']) !!}
        </div>
    </div>
    <!-- Website -->
    <div class="form-group">
        {!! Form::label('name', 'Website', ['class' => 'col-lg-2 control-label']) !!}
        <div class="col-lg-10">
            {!! Form::text('website', $value = null, ['class' => 'form-control', 'placeholder' => 'website']) !!}
        </div>
    </div>
    <!-- Select With One Default -->
    <div class="form-group">
        {!! Form::label('select', 'Sex', ['class' => 'col-lg-2 control-label'] )  !!}
        <div class="col-lg-10">
            {!!  Form::select('sex', [
                \App\User::SEX_MALE => ucfirst(\App\User::SEX_MALE),
                \App\User::SEX_FEMALE => ucfirst(\App\User::SEX_FEMALE)
                ],  'sex', ['class' => 'form-control' ]) !!}
        </div>
    </div>
    <!-- Submit Button -->
    <div class="form-group">
        <div class="col-lg-10 col-lg-offset-2">
            {!! Form::submit('Submit', ['class' => 'btn btn-lg btn-info pull-right'] ) !!}
        </div>
    </div>

</fieldset>

{!! Form::close()  !!}
</div>
