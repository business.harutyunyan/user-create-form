<?php

return [
    'userNotCreated' => 'The user not created',
    'userCreatedSuccessfully' => 'The user %s was created successfully',
];
